NAME = libETC1

SOURCES = frameworks/native/opengl/libs/ETC1/etc1.cpp
OBJECTS = $(SOURCES:.cpp=.o)

CXXFLAGS += -std=gnu++2a
CPPFLAGS += -Iframeworks/native/opengl/include

debian/out/frameworks/native/$(NAME).a: $(OBJECTS)
	mkdir -p debian/out/frameworks/native
	ar -rcs $@ $^

$(OBJECTS): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
